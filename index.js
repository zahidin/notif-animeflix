const request = require('request')
const connect = require('./config')
const cron = require('node-cron')

let date = new Date()
let d = date.getDay()

const sendMsg = (device , message) =>{
    let restKey = 'OWU4MzQ2NjctZDA1NC00ZTFhLThlNmQtM2Q3MGFhYjdjZDA0'
    let appId = '07d03c16-6f65-4330-8bad-4194b81483cb'
    request(
        {
            method:'POST',
            uri:'https://onesignal.com/api/v1/notifications',
            headers: {
                "authorization":`Basic ${restKey}`,
                "content-type":"application/json"
            },
            json:true,
            body:{
                'app_id':appId,
                'contents':{en: message},
                'include_player_ids': Array.isArray(device) ? device : [device]
            }
        },
        (err, response, body) => {
            if(!body.errors){
                console.log(body)
            }else{
                console.error('Error :',body.errors)
            }
        }
    )
}

const task = cron.schedule('* * 10 * * 6', ()=>{
connect.Connect.query(`SELECT users.app_id,favourites.name_series from users INNER JOIN favourites ON users.id = favourites.user_id WHERE users.app_id != 'website'`,(err,result,field) => {
    if (err) throw err
    result.map((val,key) => {
        connect.Connect.query(`SELECT COUNT(*) as jumlah FROM videos WHERE series = '${val.name_series}'`,(err2,result2,fields2)=>{
            let jml = result2[0].jumlah
            sendMsg(val.app_id, `Halo Animers !!! kami ingin ngasih tau kalo series ${val.name_series} favoritmu sedang di episode ${jml}`);
        })
    })
})
},{
	scheduled:false
})

task.start()


